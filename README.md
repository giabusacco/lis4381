> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Gia Busacco

### LIS4381 Requirements:

*Course Work Links:*

1. [A1_README.md](a1/A1_README.md "My A1 README.md file")
    - Install AMPPS ("ONLY" if not previously installed!)
    - Install JDK 
    - Install Android Studio and create My First App
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions

2. [A2_README.md](a2/A2_README.md "My A2 README.md file")
    - Create Healthy Recipes Android app
    - Provide screenshots of completed app
    - Provide screenshots of skillsets

3. [A3_README.md](a3/A3_README.md "My A3 README.md file")
    - Screenshot of ERD
    - Screenshot of running application's first user interface
    - Screenshot of running applications second user interface
    - Links to following files:
        - a3.mwb
        - a3.sql

4. [A4_README.md](a4/A4_README.md "My A4 README.md file")
    - Create a Bootstrap carousel
    - Provide screenshots for the carousel and data validation (both passing and failing validation)
    - Provide screenshots of skillsets 10-12

5. [A5_README.md](a5/A5_README.md "My A5 README.md file")
    - Develop server-side validation of a form in PHP
    - Develop a form that adds data into a database
    - Provide screenshots for skillsets 13-15

6. [P1_README.md](p1/P1_README.md "My P1 README.md file")
    - Screenshot of running application's first user interface
    - Screenshot of running application's second user interface
    - Screenshots of running Skillsets 7, 8, 9

7. [P2_README.md](p2/P2_README.md "My P2 README.md file")
    - Screenshot of failed validation
    - Screenshots of before *and* after successful edit
    - Screenshot of deleted record
    - Screenshot of RSS feed


