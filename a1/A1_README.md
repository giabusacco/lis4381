> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Gia Busacco

### Assignment 1 Requirements:

*Four Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chs 1, 2)
4. Bitbucket repo links: a) this assignment and b) the completed tutorial above (bitbucketstationlocations).

#### README.md file should include the following items:

* Screenshot of AMPPS Installation [My PHP Installation](http://localhost/cgi-bin/phpinfo.cgi "PHP Localhost");
* Screenshot of running Java Hello;
* Screenshot of running Android Studio - My First App;
* Git commands w/short description;

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - create an empty git repository or reinitialize an existing repository
2. git status - check walking tree status 
3. git add - add new and changed files to the staging area
4. git commit - commit changes to the repository
5. git push - push changes to remote repository
6. git pull - fetch from and integrate with another repository or local branch
7. git log - view changes of running record of commits

#### Assignment Screenshots:

| *Screenshot of AMPPS running (http://localhost/cgi-bin/phpinfo.cgi)*: | *Screenshot of running Java Hello*:                 |
|-----------------------------------------------------------------------|-----------------------------------------------------|
| ![AMPPS Installation Screenshot](img/ampps.png)                       | ![JDK Installation Screenshot](img/jdk_install.png) |

| *Screenshot of Android Studio - My First App*: |   |
|---|---|
| ![Android Studio Installation Screenshot](img/Nexus_HelloWorld.png) | ![Android Studio Installation Screenshot](img/Pixel_HelloWorld.png) |

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://gb16j@bitbucket.org/giabusacco/bitbucketstationlocations.git "Bitbucket Station Locations")
