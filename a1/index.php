<!DOCTYPE html>
<html lang="en">
  <head>
    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that displays my work and projects I have created thus far. These examples of mine showcase my skills and abilities with different coding languages and applications.">
		<meta name="author" content="Gia Busacco">
    	<link rel="icon" href="favicon.ico">

		<title>LIS4381- Assignment1</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> What is expected of professional Web developers, programmers, software engineers, and team design/development is to be able to work within the confines of some kind of version control system to store and maintain files. 
					Throughout the course LIS4381: Mobile Web Application Development, the Git distributed version control system will be used to manage assignment files and projects. With Assignment 1, I learned how to use Bitbucket to manage Git repositories and Git command line tools.
				</p>

				<h4>Java Installation</h4>
				<img src="img/jdk_installation.png" class="img-fluid" alt="JDK Installation">

				<h4>Android Studio My First App</h4>
				<img src="img/Nexus_HelloWorld.png" class="img-responsive center-block" alt="Android Studio Installation">

				<h4>AMPPS Installation</h4>
				<img src="img/ampps.png" class="img-responsive center-block" alt="AMPPS Installation">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
