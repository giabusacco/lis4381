> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Gia Busacco

### Assignment 2 Requirements:

*Two Parts:*

1. Create a mobile recipe app using Android Studio 
2. Complete Skillset 1, Skillset 2, and Skillset 3

#### README.md file should include the following items:

* Screenshot of running application's first user interface
* Screenshot of running application's second user interface
* Screenshot of Skillset 1
* Screenshot of Skillset 2 
* Screenshot of Skillset 3

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.

#### Assignment Screenshots:

| *Screenshot of Nexus 5 first user interface running*: | *Screenshot of Nexus 5 second user interface running*: | 
|---|---|
| ![Nexus 5 First User Interface Screenshot](img/nexus5_1.png) | ![Nexus 5 Second User Interface Screenshot](img/nexus5_2.png) |

|*Screenshot of running Skillset 1*:|*Screenshot of running Skillset 2*:|*Screenshot of running Skillset 3*:|
|-|-|-|
|![Skillset 1 Screenshot](img/evenOdd_ss.png)|![Skillset 2 Screenshot](img/LargestNumber_ss.png)|![Skillset 3 Screenshot](img/ArrayLoop_ss.png)|
