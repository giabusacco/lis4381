<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that displays my work and projects I have created thus far. These examples of mine showcase my skills and abilities with different coding languages and applications.">
		<meta name="author" content="Gia Busacco">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment2</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Requirements:</strong> To create a Healthy Recipes Android app and provide screenshots of the completed and running app. 
					Additionally, three running screenshots are provided of these respective skillsets: Even or Odd integer, Largest Integer, & Loop Statement. 
				</p>

				<h4>Healthy Recipes App</h4>
				<img src="img/nexus5_1.png" class="img-responsive center-block" alt="Healthy Recipes App">
				<img src="img/nexus5_2.png" class="img-responsive center-block" alt="Healthy Recipes App">


				<h4>Running Skillset 1</h4>
				<img src="img/evenOdd_ss.png" class="img-responsive center-block" alt="Skillset 1">

				<h4>Running Skillset 2</h4>
				<img src="img/LargestNumber_ss.png" class="img-responsive center-block" alt="Skillset 2">
				
				<h4>Running Skillset 3</h4>
				<img src="img/ArrayLoop_ss.png" class="img-responsive center-block" alt="Skillset 3">

				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
