> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Gia Busacco

### Assignment 3 Requirements:

*4 Items:*

1. Screenshot of ERD
2. Screenshot of running application's first user interface
3. Screenshot of running applications second user interface
4. Links to following files:
    a. a3.mwb
    b. a3.sql


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
#### Assignment Screenshots:

|*Screenshot of first user interface*:|*Screenshot of second user interface*:|
|-|-|
|![First My Event User Interface](img/myevent.png)|![Second My Event User Interface](img/myevent2.png)|
|-|-|
|*Screenshot of ERD*:|*Screenshot of MWB*:|
|-|-|
|![ERD Screenshot](img/erd.png)|![MWB Screenshot](img/erd2.png)|
|-|-|
|*Screenshot of Skillset 4*:|*Screenshot of Skillset 5*:|
|-|-|
|![Screenshot of Skillset 4](img/skillset4.png)|![Screenshot of Skillset 5](img/skillset5.png)|
|-|-|
|*Screenshot of Skillset 6*|
|![Screenshot of Skillet 6](img/skillset6.png)|

*Link to a3.mwb:*
[a3.mwb](https://bitbucket.org/giabusacco/lis4381/src/master/a3/docs/a3.mwb "a3.mwb")

*Link to a3.sql:*
[a3.sql](https://bitbucket.org/giabusacco/lis4381/src/master/a3/docs/a3.sql "a3.sql")
















