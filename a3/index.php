<!DOCTYPE html>
<html lang="en">
  <head>
    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that displays my work and projects I have created thus far. These examples of mine showcase my skills and abilities with different coding languages and applications.">
		<meta name="author" content="Gia Busacco">
    	<link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment3</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Requirements:</strong> Create a Android mobile app that allows the user enter a number of tickets desired, in which a price is calculated for the respective band and quantity.
					A pet store owner, who owns a number of pet stores, requests that you develop a Web application whereby he and his team 
					can record, track, and maintain relevant company data, based upon the following business rules:
					<p> </p>
					<p style="text-align:left;"> 1. A customer can buy many pets, but each pet, if purchased, is purchased by only one customer.</p>
					<p style="text-align:left;">2. A store has many pets, but each pet is sold by only one store.</p>
					<strong>Important note:</strong> an organization's business rules are the key to a well-designed database!
					<p> </p>
					<p style="text-align:left;"> Additionally, below are screenshots provided of the following skillsets: Descision Structures, Random Array: Integer Min and Max Value, & Methods: Enter Name and Age.</p>
					<p> </p>
					<p> </p>
					<p style="text-align:left;"> Links to ERD: </p>
					<p style="text-align:left;">1. <a href="https://bitbucket.org/giabusacco/lis4381/src/master/a3/docs/a3.mwb">A3 MWB</a></p>
					<p style="text-align:left;">2. <a href="https://bitbucket.org/giabusacco/lis4381/src/master/a3/docs/a3.sql">A3 SQL</a></p>
				</p>

				<h4>ERD</h4>
				<img src="img/erd.png" class="img-responsive center-block" alt="ERD">

				<h4>My Event App</h4>
				<img src="img/myevent.png" class="img-responsive center-block" alt="My Event App">
				<img src="img/myevent2.png" class="img-responsive center-block" alt="My Event App">

				<h4>Running Skillset 4</h4>
				<img src="img/skillset4.png" class="img-responsive center-block" alt="Skillset 4">
				
				<h4>Running Skillset 5</h4>
				<img src="img/skillset5.png" class="img-responsive center-block" alt="Skillset 5">

				<h4>Running Skillset 6</h4>
				<img src="img/skillset6.png" class="img-responsive center-block" alt="Skillset 6">

				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
