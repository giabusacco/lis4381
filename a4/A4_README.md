> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Gia Busacco

### Assignment 4 Requirements:

*5 Items:*

1. cd to local repos subdirectory
2. Clone assignment starter files
3. Review subdirectories and files
4. Open index.php and review code:
    - Suitably modify meta tags
    - Change title, navigation links, and header tags appropriately
    - Add form controls to match attributes of petstore entity
    - Add the following jQuery validation and regular experssions-- as per the entity attribute requirements (and screenshots below)
        - *All* input fields, except Notes are required
        - Use min/max jQuery validation
        - Use regexp to only allow appropriate characters for each control
    - *After* testing jQuery validation, use HTML5 property to limit the number of characters for each control
5. Create a favicon using *your* initials, and place it in each assignment's main directory, inlcuding lis4381

#### README.md file should include the following items:

* Course title, your name, assignment requirements
* Screenshots of main page, failed validation, & passed validation.
* Link to local lis4381 web app: http://localhost/repos/lis4381/index.php


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

|*Screenshot of LIS4381 Portal (Main Page)*:| |
|-|-|
|![LIS4381 Portal (Main Page)](img/mainpage.png)| |
|*Screenshot of Passed Validation*:|*Failed Validation*:|
|![Passed Validation](img/passed_val.png)|![Failed Validation](img/failed_val.png)|
|*Screenshot of Skillset 10*:|*Screenshot of Skillset 11 (wrong entry)*:|
|![Screenshot of Skillset 10](img/ss10.png)|![Screenshot of Skillset 11 (wrong entry)](img/ss11_1.png)|
|*Screenshot of Skillset 11 (correct entry)*:|*Screenshot of Skillset 12*:|
|![Screenshot of Skillet 11 (correct entry)](img/ss11_2.png)|![Screenshot of Skillet 12](img/ss12.png) |

