> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Gia Busacco

### Assignment 5 Requirements:

*Four Parts:*

1. Develop server-side validation of a form in PHP
2. Develop a form that adds data into a database
3. Chapter Questions (Chs 11, 12, 19)
4. Skillsets 13-15

#### README.md file should include the following items:

* Screenshot of Assignment 5 Pet Store data
* Screenshot of Pet Store server-side validation
* Screenshots of skillsets 13-15
* Link to Online Portfolio 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

|*index.php*:|*add_petstore_process.php*:|
|-|-|
|![index.php](img/index_php.png)|![add_petstore_process.php](img/add_petstore.png)|
|*Skillset 13: Sphere_Volume_Calculator*|*Skillset 14: Simple Calculator*|
|![Skillset 13](img/ss13.png)|![Skillset 14](img/simple_calculator.png)|
|*Simple Calculator: Addition*|*Simple Calculator: Subtraction*|
|![Simple Calculator: Addition](img/addition.png)|![Simple Calculator: Subtraction](img/subtraction.png)|
|*Simple Calculator: Multiplication*|*Simple Calculator: Division*|
|![Simple Calculator: Multiplication](img/multiplication.png)|![Simple Calculator: Division](img/division.png)|
|*Simple Calculator: Exponentiation*|*Skillset 15: Write/Read File (1)*|
|![Simple Calculator: Exponentiation](img/exponentiation.png)|![Write/Read File](img/write_blank.png)|
|*Write/Read File (2)*| |
|![Write/Read File (2)](img/write_comment.png)| |


#### Assignment Links:

*Online Portfolio:*
[Online Portfolio](https://localhost/repos/lis4381/index.php "Online Portfolio")

