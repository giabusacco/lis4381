<?php
//comment out next line if using on remote host
$IP="local";
$options = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION);

//example:
if ($IP=="local")
{
	$dsn = 'mysql:host=localhost;port=3306;dbname=gb16j';
	$username = 'root';
	$password = 'mysql';
}

//contact your Web host for DB connection documentation
else
{
	$dsn = 'mysql:host=localhost;port=3306;dbname=yourdbname';
	$username = 'yourusername';
	$password = 'yourpassword';
}

try 
{
  //instantiate new PDO connection
  $db = new PDO($dsn, $username, $password, $options);
}

catch (PDOException $e) 
{
	//only use for testing, to avoid providing security exploits
	//after testing, create custom error message
  //echo $e->getMessage();  //display error on this page
  //$error = $e->getMessage(); 
  $error = $e->getMessage();
  include('error.php'); 
	//header('Location: error.php'); //sometimes, redirecting is needed (two trips to server)
 exit();
}
?>
