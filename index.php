<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that displays my work and projects I have created thus far. These examples of mine showcase my skills and abilities with different coding languages and applications.">
		<meta name="author" content="Gia Busacco">
		<link rel="icon" href="favicon.ico">

		<title>My Online Portfolio</title>

		<?php include_once("css/include_css.php"); ?>	

		<!-- Carousel styles -->
		<style type="text/css">
		 h2
		 {
			 margin: 0;     
			 color: #000000;
			 padding-top: 50px;
			 font-size: 52px;
			 font-family: "impact", sans-serif;    
		 }
		 .item
		 {
			 background: #000000;    
			 text-align: center;
			 height: 300px !important;
		 }
		 .item img {
  				width: 100%
 				object-fit: cover !important;
			}
		 .carousel
		 {
			 margin: 20px 0px 20px 0px;
		 }
		 .bs-example
		 {
			 margin: 20px;
		 }
		</style>

	</head>
	<body>

		<?php include_once("global/nav_global.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>

				<!-- Start Bootstrap Carousel  -->
				<div class="bs-example">
					<div
						id="myCarousel"
								class="carousel"
								data-interval="1000"
								data-pause="hover"
								data-wrap="true"
								data-keyboard="true"			
								data-ride="carousel">
						
    				<!-- Carousel indicators -->
						<ol class="carousel-indicators">
							<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
							<li data-target="#myCarousel" data-slide-to="1"></li>
							<li data-target="#myCarousel" data-slide-to="2"></li>
						</ol>   
						<!-- Carousel items -->
						<div class="carousel-inner">

							<!-- -Note: you will need to modify the code to make it work with *both* text and images.  -->
							<div class="active item" style="background: url(img/keyboard.jpeg) no-repeat left center; background-size: cover;">
								<div class="container">
									<div class="carousel-caption">
										<h3>Hi, my name is Gia Busacco!</h3>
										<p class="lead">Take a look into my online portfolio to see some of the work I have created.</p>
									</div>
								</div>
							</div>

							<div class="item" style="background: url(img/nyc.jpeg) no-repeat left center; background-size: cover;">
									<div class="carousel-caption">
										<h3>My Goal</h3>
										<p>I hope to move to New York City after I graduate from FSU in Spring 2021. My dream job is a UX designer in E-Commerce. </p>									
									</div>
								</div>

								<div class="item" style="background: url(img/code.jpeg) no-repeat left center; background-size: cover;">
									<div class="carousel-caption">
										<h3>Programming Languages</h3>
										<p>The coding languages I know are Java, Javascript, PHP, and SQL.</p>								
									</div>
								</div>

							</div>
							<!-- Carousel nav -->
							<a class="carousel-control left" href="#myCarousel" data-slide="prev">
								<span class="glyphicon glyphicon-chevron-left"></span>
							</a>
							<a class="carousel-control right" href="#myCarousel" data-slide="next">
								<span class="glyphicon glyphicon-chevron-right"></span>
							</a>
							</div>
						</div>
						<!-- End Bootstrap Carousel  -->
						
						<?php
						include_once "global/footer.php";
						?>

					</div> <!-- end starter-template -->
</div> <!-- end container -->

		<?php include_once("js/include_js.php"); ?>	
	
</body>
</html>
