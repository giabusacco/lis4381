> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 

## Gia Busacco

### Project 1 Requirements:

*Four Parts:*

1. Create a launcher icon and display it in both activities (screens)
2. Must add background color(s) to both activities
3. Must add border around image and button
4. Must add text shadow (button)

#### README.md file should include the following items:

* Course title, my name, assignment requirements;
* Screenshot of running application's first user interface;
* Screenshot of running application's second user interface;


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

|*Screenshot of first user interface:*|*Screenshot of second user interface:*|-|
|-|-|-|
|![First User Interface Screenshot](img/ui_1.png)|![Second User Interface Screenshot](img/ui_2.png)|-|
|-|-|-|
|*Screenshot of runnning Skillset 7*|*Screenshot of running Skillset 8*|*Screenshot of running Skillset 9*|
|-|-|-|
|![Skillshot 7 Screenshot](img/ss7.png)|![Skillshot 8 Screenshot](img/ss8.png)|![Skillshot 9 Screenshot](img/ss9.png)|