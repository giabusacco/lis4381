<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that displays my work and projects I have created thus far. These examples of mine showcase my skills and abilities with different coding languages and applications.">
		<meta name="author" content="Gia Busacco">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Project1</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Requirements:</strong> Backward-Engineer to create a My Business Card App using my own headshot photo, contact information, and interests.
					<p style="text-align:left;">1. Create a launcher icon and display it in both activities (screens)</p>
					<p style="text-align:left;">2. Must add background color(s) to both activities</p>
					<p style="text-align:left;">3. Must add border around image and button</p>
					<p style="text-align:left;">4. Must add text shadow (button)</p>
				</p>

				<h4>My Business Card App</h4>
				<img src="img/ui_1.png" class="img-responsive center-block" alt="My Business Card App">
				<img src="img/ui_2.png" class="img-responsive center-block" alt="My Business Card App">

				<h4>Skillset 7</h4>
				<img src="img/ss7.png" class="img-responsive center-block" alt="Skillset 7">

				<h4>Skillset 8</h4>
				<img src="img/ss8.png" class="img-responsive center-block" alt="Skillset 8">
				
				<h4>Skillset 9</h4>
				<img src="img/ss9.png" class="img-responsive center-block" alt="Skillset 9">

				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
