> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Gia Busacco

### Project 2 Requirements:

*Four Parts:*

1. Add an edit function to pet store database
2. Add a delete function to pet store database
3. Add an update function to pet store database
4. Create an RSS feed

#### README.md file should include the following items:

* Screenshot of failed validation
* Screenshots of before *and* after successful edit
* Screenshot of deleted record
* Screenshot of RSS feed

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

|*Screenshot of Carousel*:|*Screenshot of index.php*:|
|-|-|
|![Carousel](img/carousel.png)|![index.php](img/index.png)|
|*Screenshot of edit_petstore.php*:|*Screenshot of Failed Validation*:|
|![edit_petstore.php](img/editPetstore.png)|![Failed Validation](img/error_validation.png)|
|*Screenshot of Passed Validation*:|*Screenshot of Delete Record Prompt*:|
|![Passed Validation](img/passed_validation.png)|![Delete Record Prompt](img/delete_record.png)|
|*Screenshot of RSS Feed*:| |
|![RSS Feed](img/rss.png)| |


#### Assignment Links:

*Online Portfolio:*
[Online Portfolio](https://localhost/repos/lis4381/index.php "Online Portfolio")


