public class ArraysAndLoops
 {
    public static void main (String args[])
    {
        System.out.println("Developer: Gia Busacco");
        System.out.println("Program loops through array of strings.");
        System.out.println("Use following values: dog, cat, bird, fish, insect.");
        System.out.println("Use following loop structures: for, enhanced for, while, do...while.");
        System.out.println();
        System.out.println("Note: Pretest loops: for, enhanced, while. Posttest loop: do...while.");
        System.out.println();

        //set up the arrray
        String[] animals = {"dog", "cat", "bird", "fish", "insect"};

        //for loop to print the array
        System.out.println("for loop:");
        for (int i=0; i < animals.length; i++){
            System.out.println(animals[i]);
        }

        //insert blank line
        System.out.println(); 

        //enhanced for loop to print the array
        System.out.println("Enhanced for loop:");
        for (String x : animals) {
            System.out.println(x);
        }

        //insert blank line
        System.out.println(); 

        //while loop to print the array
        System.out.println("while loop");
        int y = 0;
        while (y < animals.length) {
            System.out.println(animals[y]);
            y++;
        }

        //insert blank line
        System.out.println();

        //do...while loop to print the array
        System.out.println("do...while loop: ");
        int z = 0;
        do {
            System.out.println(animals[z]);
            z++;
        } while (z < animals.length);
        System.out.println();
    }//end main
}
