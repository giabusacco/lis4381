import java.util.Scanner;

public class Methods 
{
    public static void getRequirements()
    {
        System.out.println("Nested Structures\n");

        System.out.println("Program searches for user-entered integer w/in array of user-entered integers.");
        System.out.println("Create an array based upon user-entered array size *and* values.");
        System.out.println();
    }

    public static void createArray()
    {
        Scanner sc = new Scanner(System.in);
        int search = 0;
        int arraySize = 0;

        System.out.print("Enter number of integers for array (min1): ");
        arraySize = sc.nextInt();

        int nums[] = new int[arraySize];

        System.out.println("Array length: " + nums.length);

        System.out.println();

        for(int i = 0; i < arraySize ; i++)
            {
                System.out.print("Enter integer " + (i + 1 ) + ": ");
                nums[i] = sc.nextInt();
            }

        System.out.print("\nEnter search value: ");
        search = sc.nextInt();

        System.out.println();
        for (int i = 0; i < nums.length; i++)
        {
            if (nums[i] == search)
                {
                    System.out.println(search + " found at index " + i);
                }
            else
                {
                    System.out.println(search + " *not* found at index " + i);
                }    
        }
    }
}

