import java.util.Scanner;

public class EvenOrOdd
{
  public static void main(String[] args) {
    
    // print to screen user prompt
    System.out.println("Developer: Gia Busacco");
    System.out.println("Program evaluates integers as even or odd.");
    System.out.println("Note: Program does *not* check for non-numeric characters.");
    System.out.println(); // pritns blank line for spacing

    // intialize variables, create Scanner object, capture user input
    int x = 0;
    System.out.println("Enter integer: ");
    Scanner sc = new Scanner(System.in);
      x = sc.nextInt();

    /* If number is divisible by 2 then it's an even number
     * else odd number*/
    if ( x % 2 == 0 )
      {
        System.out.println(" is an even number.");
      }
        
     else
        {
          System.out.println(" is an odd number");
        }
  }
}
