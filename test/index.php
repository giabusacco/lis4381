<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <title>Using RSS Feeds</title>
    </head>
    <body>
    <?php

    $html = "";
    $publisher = "BBC World News";
    $url = "http://feeds.bbci.co.uk/news/world/us_and_canada/rss.xml?edition=int";

    $html .='<h2>'.$publisher .'</h2>';
    $html .=$url;

    $rss = simplexml_load_file($url);
    $count = 0;
    $html .='<url>';
    foreach($rss->channel->item as $item)
    {
        $count++;
        if($count > 10)
        {
        break;
        }
        $html .='<li><a href="'.htmlspecialchars($item->link).'">'.htmlspecialchars($item->title).'</a><br/>';
        $html .=htmlspecialchars($item->description).'<br/>';
        $html .=htmlspecialchars($item->pubDate).'</li><br/>';
    }
    $html .='</ul>';

    print $html;
    ?>
</body>